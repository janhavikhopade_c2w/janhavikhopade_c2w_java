class SwitchDemo{
	public static void main (String[] args){
		int num = 2;
		System.out.println("Before Switch ");
		switch (num){
			case 1:
				System.out.println("one");
			case 2:
				System.out.println ("Two");
			case3:
				System.out.println("Three");
			default :
				System.out.println ("in default state");
		}
		System.out.println("after switch");
	}
}
