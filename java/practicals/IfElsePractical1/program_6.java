//to check number is divisible by 3 or 7


class Division2{
	public static void main(String[] args){
		int num = 20;
		if( num % 3 == 0 || num % 7 == 0){
			System.out.println("Number is divisible by 3 or 7");
		}
		else{
			System.out.println("Number is not divisible by 3 or 7");
		}
	}
}
