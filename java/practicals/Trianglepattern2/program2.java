
import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.print(" Enter rows");
		int rows = Integer.parseInt(br.readLine());

		for( int i= 1; i<=rows ; i++){
			if (i%2 == 1){
				char ch = 'a';
				for (int j =1 ; j<=i; j++){
					System.out.print(ch+ " ");
					ch ++;
				}
				System.out.println();
			}
			else {
				for ( int j=1 ; j<=i; j++){
					System.out.print("$" + " ");
				}

				System.out.println();
			}
		}
	}
}
