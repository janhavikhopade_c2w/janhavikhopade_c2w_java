class C2W{
	public static void main (String[] args){
		int num = 214367689;
		int oddCount = 0;
		int evenCount = 0;
		while (num >0){
			int rem = num % 10;
			num = num/ 10;
			if (rem% 2==1){
				oddCount++;
			}
			else{
				evenCount++;
			}
		}
		System.out.println("Odd Count: "+oddCount);
		System.out.println("Even Count: " + evenCount);
	}
}
