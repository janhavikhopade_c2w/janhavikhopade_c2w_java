//to check student passed with first class distinction ,first class , second class and pass


class Result{
	public static void main (String[] args){
		float percentage = 78;
		if ( percentage >= 75.00){
			System.out.println("Passed: First class with distinction");
		}
		else if (percentage >= 60.00){
			System.out.println(" Passed: First class");
		}
		else if (percentage >= 50.00){
			System.out.println(" Passed: Second class");
		}
		else if ( percentage >= 35.00){
			System.out.println(" Passed");
		}else {
			System.out.println ("invalid input");
		}
	}
}
