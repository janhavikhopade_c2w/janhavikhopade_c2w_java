//Realtime example of if else 

class RealtimeEx{
	public static void main (String[] args){
		double temperature = 12.00;
		if (temperature < 0){
		       System.out.println (" its freezing outside");
		}
		else if ( temperature >= 0 && temperature < 10){
			System.out.println(" its cold ");
		}
		else if ( temperature > 10 && temperature < 20){
			System.out.println(" its cool");
		}
		else if ( temperature > 20 && temperature <30){
			System.out.println ("its warm");
		}
		else {
			System.out.println("its hot");
		}
	}
}	
