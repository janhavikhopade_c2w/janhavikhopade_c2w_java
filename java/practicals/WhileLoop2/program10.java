class C2W{
	public static void main(String[] args){

		int num = 9367924;
		int sum = 0;
		int product =1 ;
		while(num>0){
			int rem = num % 10;
			num = num/10;
			if (rem %2==1 ){
				sum +=rem;
			}else{
				product *=rem;
			}
		}
		System.out.println("sum of odd digits: "+sum);
		System.out.println("Product of even digits : "+product);
	}
}
